/*
console.log('')
console.log('Soal 1')
console.log('')
console.log('LOOPING PERTAMA')
var flag = 2;
while(flag <= 20) {
  console.log(flag + ' I Love Coding');
  flag += 2;
  //flag = flag + 2; // flag += 2;
  //flag++; 
}
console.log('LOOPING KEDUA')
var flag = 20; 
while(flag >= 2) {
  console.log(flag + ' I will become a mobile developer');
  flag = flag - 2; // flag -= 2;
  //flag--; 
}

console.log('')
console.log('Soal 2')
console.log('')
for(var angka = 1; angka <= 20; angka++) {
    if ( angka%3 == 0 && angka%2 == 1)  {     // apakah kelipatan 3 & ganjil
      console.log( angka + ' - I Love Coding')
    } else if ( angka%2 == 0 ) { // genap
      console.log( angka + ' - Berkualitas')
    } else if ( angka%2 == 1 ) { // ganjil
      console.log( angka + ' - Santai')
    } 
}

console.log('')
console.log('Soal 3')
console.log('')
// buat ngeprint baris
for(var baris = 1; baris <= 4; baris++) {
  // buat ngeprint kolom
  var tampungKolom = ''; // ini untuk tampung kolom sebelum dicetak (string)
  for(var kolom = 1; kolom <= 8; kolom++) {
    tampungKolom = tampungKolom + '#';
  }
  console.log(tampungKolom);
}

console.log('')
console.log('Soal 4')
console.log('')
// buat ngeprint baris
for(var baris = 1; baris <= 7; baris++) {
  // buat ngeprint kolom
  var tampungKolom = ''; // ini untuk tampung kolom sebelum dicetak (string)
  for(var kolom = 1; kolom <= baris; kolom++) {
    tampungKolom += '#';
  }
  console.log(tampungKolom);
}
*/

console.log('')
console.log('Soal 5')
console.log('')
// buat ngeprint baris
for(var baris = 0; baris < 8; baris++) {
  if (baris%2 == 1) {
    // buat ngeprint kolom ganjil
    var tampungKolomGanjil = ''; // ini untuk tampung kolom sebelum dicetak (string)
    for(var kolom = 1; kolom <= 4; kolom++) {
      tampungKolomGanjil += '# ';
    }
    console.log(tampungKolomGanjil);
  } else {
    // buat ngeprint kolom genap
    var tampungKolomGenap = ''; // ini untuk tampung kolom sebelum dicetak (string)
    for(var kolom = 1; kolom <= 4; kolom++) {
      tampungKolomGenap += ' #';
    }
    console.log(tampungKolomGenap);
  }
}



