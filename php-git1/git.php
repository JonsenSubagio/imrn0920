<?php
function tentukan_nilai($number)
{
    $output = '';

            if ( $number >= 85 && $number <= 100 ) {
                $output = "Sangat Baik";
            } else if ( $number >= 70 && $number <= 85) {
                $output = "Baik";
            } else if ( $number >= 60 && $number <= 70 ) {
                $output = "Cukup";
            } else 
                $output = "Kurang";

            return $output. "<br>";
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
echo "<br>";
echo "<br>";


function ubah_huruf($string) {


$output = strtr( $string, "abcdefghijklmnopqrstuvwxyz ","bcdefghijklmnopqrstuvwxyza" );


return $output. "<br>";

}


    // TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
echo "<br>";
echo "<br>";

function tukar_besar_kecil($string) {
        
$keluaran = strtr($string, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" );

return $keluaran. "<br>";
 }
        
        // TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
?>
