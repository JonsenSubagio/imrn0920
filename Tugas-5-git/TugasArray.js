/*
console.log('')
console.log('soal 1')
console.log('')


function range(startNum, finishNum) {
    if (startNum == undefined || finishNum == undefined) {
        return -1
    } else {
        var arrayOutput = []
        if(startNum <= finishNum) {
            // mengulang angka apabila startNum < finishNum
            for(var n = startNum; n <= finishNum; n++) {
                arrayOutput.push(n)
            }
        } else {
            // mengulang angka apabila startNum > finishNUm
            for(var n = startNum; n >= finishNum; n--) {
                arrayOutput.push(n)
            }
        }
        return arrayOutput
    }
}


console.log(range(1, 10)) // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

console.log('')
console.log('soal 2')
console.log('')

function rangeWithStep(startNum, finishNum, step) {
    var arrayKeluaran = []
    if(startNum <= finishNum) {
        // mengulang angka apabila startNum < finishNum
        for(var n = startNum; n <= finishNum; n = n + step) {
            arrayKeluaran.push(n)
        }
    } else {
        // mengulang angka apabila startNum > finishNUm
        for(var n = startNum; n >= finishNum; n = n - step) {
            arrayKeluaran.push(n)
        }
    }
    return arrayKeluaran
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

*/

console.log('')
console.log('soal 3')
console.log('')


// Code di sini
function sum(angkaAwalDeret = 0, angkaAkhirDeret = 0, bedaJarak = 1) {
    // membuat array kosong
    var hasil = []

    // mengisi array
    if(angkaAwalDeret <= angkaAkhirDeret) {
        // mengulang angka apabila startNum < finishNum
        for(var n = angkaAwalDeret; n <= angkaAkhirDeret; n = n + bedaJarak) {
            hasil.push(n)
        }
    } else {
        // mengulang angka apabila startNum > finishNUm
        for(var n = angkaAwalDeret; n >= angkaAkhirDeret; n = n - bedaJarak) {
            hasil.push(n)
        }
    }

    // hasil --> yang sudah diisi
    var total = 0 // ini digunakan untuk menampung hasil penjumlahan

    // ini digunakan untuk melakukan perhitungan total
    for(var indexArray = 0; indexArray < hasil.length; indexArray++) {
        total = total + hasil[indexArray]
    }

    // ini untuk mengeluarkan hasil total yang sudah dihitung ke layar
    return total
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

/*
// index         0, 1,  2,  3,  4, 5,  6,  7,  8,  9
var percobaan = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] // ini adalah array yang akan dihitung isinya
var total = 0 // ini digunakan untuk menampung hasil penjumlahan

// ini digunakan untuk melakukan perhitungan total
for(var indexArray = 0; indexArray < percobaan.length; indexArray++) {
    //console.log(percobaan[indexArray])
    total = total + percobaan[indexArray]
}

// ini untuk mengeluarkan hasil total yang sudah dihitung ke layar
console.log(total)
*/

console.log('')
console.log('soal 4')
console.log('')

//contoh input

function dataHandling (arrayInput) {
    /*
    Nomor ID:  0001
    Nama Lengkap:  Roman Alamsyah
    TTL:  Bandar Lampung 21/05/1989
    Hobi:  Membaca
    */
    //                               4
    for(var index = 0; index < arrayInput.length; index++) {
    // index = 0
    // index = 1
    // index = 2
    // index = 3
    console.log('Nomor ID: ' + arrayInput[index][0])
    console.log('Nama Lengkap: ' + arrayInput[index][1])
    console.log('TTL: ' + arrayInput[index][2] + ' ' + arrayInput[index][3])
    console.log('Hobi: ' + arrayInput[index][4])
    console.log(' ')
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],  // a - index ke-0
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],     // b - index ke-1
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],                   // c - index ke-2
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]        // d - index ke-3
] 

dataHandling(input)

console.log('')
console.log('soal 5')
console.log('')

//                                                               0    1    2    3
// string adalah array of character, misal ada string "Halo" = ["H", "a", "l", "o"]  -->  ["o", "l", "a", "H"]
function balikKata(isi) {
    var kataDibalik = '';

    // melakukan perulangan/looping untuk mengisi variable kataDibalik
    // awalan, pengecekan, penambahan/pengurangan
    for(var index = isi.length - 1; index >= 0; index--) {
        // index 3
        // index 2
        // index 1
        // index 0
        // kataDibalik.push(isi[index]);
        kataDibalik = kataDibalik + isi[index];
    }

    // output
    return kataDibalik;
} 

console.log(balikKata("Kasur Rusak")) // kasuR rusaK 
console.log(balikKata("SanberCode")) // edoCrebnaS 
console.log(balikKata("Haji Ijah")) // hajI ijaH 
console.log(balikKata("racecar")) // racecar 
console.log(balikKata("I am Sanbers")) // srebnaS ma 
    

console.log('')
console.log('soal 5')
console.log('')

console.log('Sorry....i cannot work this and i dont know why....i already try anything to work on this but i failed it all :). So i will just gonna leave a my message here T_T')



